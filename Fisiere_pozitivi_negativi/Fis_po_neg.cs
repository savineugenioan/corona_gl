﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Globalz = Genetic_Lab_Nr_tel_PDF.Globalz;

namespace Fisiere_pozitivi_negativi
{
    public partial class Fis_po_neg : Form
    {
        string desktop_path;
        string folder_pozitivi;
        string folder_negativi;
        string[] files;
        int count;
        int count_Adev;
        public Fis_po_neg()
        {
            InitializeComponent();
            backgroundWorker1.DoWork += new DoWorkEventHandler(bkg);
        }

        private void bkg(object sender, DoWorkEventArgs e)
        {
            try
            {
                CreareFoldere();
                CopiereFisiere();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "EROARE", MessageBoxButtons.OK, MessageBoxIcon.Error);
                string x = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
                File.AppendAllText(x+ "\\" + "eroare.txt", ex.Message + Environment.NewLine +ex.StackTrace);
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            desktop_path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            textBox1.Text = Properties.Settings.Default.GetPath;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            using (var fbd = new FolderBrowserDialog())
            {
                DialogResult result = fbd.ShowDialog();
                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                {
                    textBox1.Text = fbd.SelectedPath;
                    Properties.Settings.Default.GetPath = fbd.SelectedPath;
                    Properties.Settings.Default.Save();
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text.Trim() == "")
            {
                MessageBox.Show("Selectati un folder!", "ATENTIE", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            files = Directory.GetFiles(textBox1.Text);
            progressBar1.Value = 0;
            count = 0;
            count_Adev = 0;
            foreach (var file in files)
            {
                if (file.Contains(".pdf"))
                {
                    if (file.Contains("_ADEV") || file.Contains("_adev"))
                    {
                        count_Adev++;
                    }
                    count++;
                }
            }
            progressBar1.Maximum = count;
            if (count == 0)
            {
                MessageBox.Show("Nu a fost gasit nici un fisier pdf care sa nu fie adeverinta !", "ATENTIE", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (!backgroundWorker1.IsBusy)
            {
                Globalz.LoadingGif(this.button1);
                backgroundWorker1.RunWorkerAsync();
            }
        }

        private void CopiereFisiere()
        {
            int count_pozitivi = 0;
            int count_negativi = 0;
            int contor = 0;
            foreach (var file in files)
                if (file.Contains(".pdf"))
                {
                if (!file.Contains("_ADEV") && !file.Contains("_adev"))
                {
                    if (file.Contains("(+)"))
                    {
                        File.Copy(file, folder_pozitivi + "\\" + file.Substring(file.LastIndexOf('\\')+1), true);
                        count_pozitivi++;
                    }
                    else
                    {
                        File.Copy(file, folder_negativi + "\\" + file.Substring(file.LastIndexOf('\\')+1), true);
                        count_negativi++;
                    }
                }
                contor++;
                backgroundWorker1.ReportProgress(contor);
            }
            MessageBox.Show($"Fisierele au fost copiate cu succes."+
                $" Dintr-un total de {count} fisiere, au fost {count_pozitivi} pozitivi si {count_negativi} negativi !({count_Adev} adeverinte)",
                "Succes", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void CreareFoldere()
        {
            string[] date = DateTime.Today.ToString("MM/dd/yyyy").Split(new string[] { "/","-" }, StringSplitOptions.RemoveEmptyEntries);
            string x = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            string azi = date[1] + "." + date[0] + "." + date[2];
            if (!Directory.Exists(desktop_path + "\\" + azi + "." + "pozitivi"))
            {
                Directory.CreateDirectory(desktop_path + "\\" + azi + "." + "pozitivi");
            }
            if (!Directory.Exists(desktop_path + "\\" + azi + "." + "negativi"))
            {
                Directory.CreateDirectory(desktop_path + "\\" + azi + "." + "negativi");
            }
            folder_negativi = desktop_path + "\\" + azi + "." + "negativi";
            folder_pozitivi = desktop_path + "\\" + azi + "." + "pozitivi";
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            Globalz.del_gif(this);
            progressBar1.Value = 0;
        }

        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            Globalz.del_gif(this);
            progressBar1.Value = e.ProgressPercentage;
        }
    }
}
