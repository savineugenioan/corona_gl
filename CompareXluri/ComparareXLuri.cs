﻿using Compare_ID;
using Genetic_Lab_Nr_tel_PDF;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;

namespace CompareXluri
{
    public partial class ComparareXLuri : Form
    {
        List<string> lista_XL1;
        List<string> lista_XL2;
        string desktop_path;
        public ComparareXLuri()
        {
            InitializeComponent();
            backgroundWorker1.DoWork += new DoWorkEventHandler(bkg);
        }

        private void bkg(object sender, DoWorkEventArgs e)
        {
            lista_XL1.Clear();
            lista_XL2.Clear();
            createListe();
            int max_progress = lista_XL1.Count + lista_XL2.Count;
            List<string> XL1_id_lipsa = new List<string>();
            List<string> XL2_id_lipsa = new List<string>();
            int count = 0;
            try
            {
                foreach (string x_XL1 in lista_XL1)
                {
                    count++;
                    if (!lista_XL2.Contains(x_XL1))
                    {
                        XL1_id_lipsa.Add(x_XL1);
                        max_progress++;
                    }
                    backgroundWorker1.ReportProgress((count*100)/max_progress);
                }
                foreach (string x_XL2 in lista_XL2)
                {
                    count++;
                    if (!lista_XL1.Contains(x_XL2))
                    {
                        XL2_id_lipsa.Add(x_XL2);
                        max_progress++;
                    }
                    backgroundWorker1.ReportProgress((count * 100) / max_progress);
                }
                string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\Comparare_XLuri.txt";
                if (File.Exists(path))
                {
                    File.Delete(path);
                }
                File.WriteAllText(path, String.Format("ID-uri din XL1({0}) care nu se regasesc XL2({1}):" + Environment.NewLine,textBox1.Text, textBox3.Text));
                foreach (string x_XL1 in XL1_id_lipsa)
                {
                    count++;
                    File.AppendAllText(path, x_XL1 + Environment.NewLine);
                    backgroundWorker1.ReportProgress((count * 100) / max_progress);
                }
                File.AppendAllText(path, String.Format("ID-uri din XL2({0}) care nu se regasesc in XL1({1}):" + Environment.NewLine, textBox3.Text, textBox1.Text));
                foreach (string x_XL2 in XL2_id_lipsa)
                {
                    count++;
                    File.AppendAllText(path, x_XL2 + Environment.NewLine);
                    backgroundWorker1.ReportProgress((count * 100) / max_progress);
                }
                MessageBox.Show("Comparare efectuata cu succes", "Succes", MessageBoxButtons.OK, MessageBoxIcon.Information);
                backgroundWorker1.ReportProgress(0);
            }
            catch (IOException exc)
            {
                MessageBox.Show("Verificati sa nu aveti XL-uri deschise!", "EROARE IO", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "EROARE", MessageBoxButtons.OK, MessageBoxIcon.Error);
                // Get stack trace for the exception with source file information
                if (File.Exists(desktop_path + "\\" + "erori.txt"))
                {
                    File.Delete(desktop_path + "\\" + "erori.txt");
                }
                File.AppendAllText(desktop_path + "\\" + "erori.txt", ex.StackTrace + Environment.NewLine + ex.Message);
            }

        }

        private void createListe()
        {
            object misValue = System.Reflection.Missing.Value;
            Excel.Application xlApp = new Excel.Application();
            Excel.Workbook xlWorkbook = xlApp.Workbooks.Open(textBox1.Text);
            Excel._Worksheet xlWorksheet = xlWorkbook.Sheets[1];
            Excel.Range xlRange = xlWorksheet.UsedRange;
            int rw = xlRange.Rows.Count;
            int cl = xlRange.Columns.Count;
            for (int i = 1; i <= rw; i++)
            {
                string str1 = Convert.ToString((xlRange.Cells[i, int.Parse(Properties.Settings.Default.ColCod1)] as Excel.Range).Value2);
                str1 = str1 != null ? str1.Trim() : str1;
                if (!lista_XL1.Contains(str1))
                {
                    lista_XL1.Add(str1);
                }
            }

            xlWorkbook.Close(false, misValue, misValue);
            xlApp.Quit();

            xlApp = new Excel.Application();
            xlWorkbook = xlApp.Workbooks.Open(textBox3.Text);
            xlWorksheet = xlWorkbook.Sheets[1];
            xlRange = xlWorksheet.UsedRange;
            rw = xlRange.Rows.Count;
            cl = xlRange.Columns.Count;
            for (int i = 1; i <= rw; i++)
            {
                string str1 = Convert.ToString((xlRange.Cells[i, int.Parse(Properties.Settings.Default.ColCod2)] as Excel.Range).Value2);
                str1 = str1 != null ? str1.Trim() : str1;
                if (!lista_XL2.Contains(str1))
                {
                    lista_XL2.Add(str1);
                }
            }
            Marshal.ReleaseComObject(xlRange);
            Marshal.ReleaseComObject(xlWorksheet);
            xlWorkbook.Close(false, misValue, misValue);
            Marshal.ReleaseComObject(xlWorkbook);
            xlApp.Quit();
            Marshal.ReleaseComObject(xlApp);
        }
        private void button2_Click(object sender, EventArgs e)
        {
            using (var fbd = new OpenFileDialog())
            {
                DialogResult result = fbd.ShowDialog();
                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.FileName))
                {
                    textBox1.Text = fbd.FileName;
                    Properties.Settings.Default.XL1PathID = fbd.FileName;
                    Properties.Settings.Default.Save();
                }
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            using (var fbd = new OpenFileDialog())
            {
                DialogResult result = fbd.ShowDialog();
                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.FileName))
                {
                    textBox3.Text = fbd.FileName;
                    Properties.Settings.Default.XL2PathID = fbd.FileName;
                    Properties.Settings.Default.Save();
                }
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox c = (ComboBox)sender;
            Properties.Settings.Default.ColCod1 = c.SelectedItem.ToString();
            Properties.Settings.Default.Save();
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox c = (ComboBox)sender;
            Properties.Settings.Default.ColCod2 = c.SelectedItem.ToString();
            Properties.Settings.Default.Save();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            progressBar1.Value = 0;
            progressBar1.Maximum = 100;
            if (!backgroundWorker1.IsBusy)
            {
                Globalz.LoadingGif(this.button1);
                backgroundWorker1.RunWorkerAsync();
            }
        }

        private void ComparareXLuri_Load(object sender, EventArgs e)
        {
            LoadCombo();
            lista_XL1 = new List<string>();
            lista_XL2 = new List<string>();
            textBox1.Text = Properties.Settings.Default.XL1PathID;
            textBox3.Text = Properties.Settings.Default.XL2PathID;
            desktop_path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            checkbtn();
        }

        private void LoadCombo()
        {
            for (int i = 1; i <= 22; i++)
            {
                comboBox1.Items.Add(i);
            }
            if (!string.IsNullOrWhiteSpace(Properties.Settings.Default.ColCod1.ToString()))
            {
                comboBox1.SelectedItem = int.Parse(Properties.Settings.Default.ColCod1);
            }
            else
            {
                comboBox1.SelectedIndex = 0;
            }

            for (int i = 1; i <= 22; i++)
            {
                comboBox2.Items.Add(i);
            }
            if (!string.IsNullOrWhiteSpace(Properties.Settings.Default.ColCod2.ToString()))
            {
                comboBox2.SelectedItem = int.Parse(Properties.Settings.Default.ColCod2);
            }
            else
            {
                comboBox2.SelectedIndex = 0;
            }
        }

        private void checkbtn()
        {
            if (textBox1.Text.Trim() == "" || textBox3.Text.Trim() == "")
            {
                button1.Enabled = false;
            }
            else
            {
                button1.Enabled = true;
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            checkbtn();
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            checkbtn();
        }

        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if (e.ProgressPercentage <= progressBar1.Maximum)
                progressBar1.Value = e.ProgressPercentage;
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            Globalz.del_gif(this);
            progressBar1.Value = 0;
        }
    }
}
