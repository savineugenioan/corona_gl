﻿using Compare_ID;
using Genetic_Lab_Nr_tel_PDF;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;

namespace cOMPARE_iD
{
    public partial class Compare_ID : Form
    {
        List<string> lista_XL;
        List<string> lista_PDF;
        List<string> files;
        string outputFolder;
        string desktop_path;
        public Compare_ID()
        {
            InitializeComponent();
            backgroundWorker1.DoWork += new DoWorkEventHandler(bkg);
        }

        private bool checkPdf(string name)
        {
            return name.Contains(".pdf");
        }
        private void bkg(object sender, DoWorkEventArgs e)
        {
            List<string> XL_id_lipsa = new List<string>();
            List<string> PDF_id_lipsa = new List<string>();
            string[] date = DateTime.Today.ToString("MM/dd/yyyy").Split(new string[] { "/", "-" }, StringSplitOptions.RemoveEmptyEntries);
            string azi = date[1] + "." + date[0] + "." + date[2];
            outputFolder = desktop_path + "\\" + azi + "." + "CP";
            if (Directory.Exists(outputFolder))
            {
                Directory.Delete(outputFolder, true);
            }
            Directory.CreateDirectory(outputFolder);
            try
            {
                lista_XL.Clear();
                lista_PDF.Clear();
                createListe();
                foreach(string x_XL in lista_XL)
                {
                    if (!lista_PDF.Contains(x_XL))
                    {
                        XL_id_lipsa.Add(x_XL);
                    }
                    else
                    {
                        files.ForEach(x => { if (x.Contains(x_XL)) { File.Copy(x,outputFolder+@"\"+ x.Substring(x.LastIndexOf('\\') + 1).Trim(), true); }  });
                    }
                }
                foreach (string x_PDF in lista_PDF)
                {
                    if (!lista_XL.Contains(x_PDF))
                    {
                        PDF_id_lipsa.Add(x_PDF);
                    }
                }
                string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop)+"\\Comparare_ID.txt";
                if (File.Exists(path))
                {
                    File.Delete(path);
                }
                File.WriteAllText(path, "ID-uri din XL care nu se regasesc in Folderul cu PDF-uri:" + Environment.NewLine);
                foreach (string x_XL in XL_id_lipsa)
                {
                    File.AppendAllText(path,x_XL + Environment.NewLine);
                }
                File.AppendAllText(path, "ID-uri din Folderul cu PDF-uri care nu se regasesc in XL:" + Environment.NewLine);
                foreach (string x_PDF in PDF_id_lipsa)
                {
                    File.AppendAllText(path, x_PDF + Environment.NewLine);
                }
                MessageBox.Show("Comparare efectuata cu succes", "Succes", MessageBoxButtons.OK, MessageBoxIcon.Information);
                backgroundWorker1.ReportProgress(0);
            }
            catch (IOException exc)
            {
                MessageBox.Show("Verificati sa nu aveti PDF-uri deschise!", "EROARE IO", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "EROARE", MessageBoxButtons.OK, MessageBoxIcon.Error);
                // Get stack trace for the exception with source file information
                if (File.Exists(desktop_path + "\\" + "erori.txt"))
                {
                    File.Delete(desktop_path + "\\" + "erori.txt");
                }
                File.AppendAllText(desktop_path + "\\" + "erori.txt", ex.StackTrace + Environment.NewLine + ex.Message);
            }

        }
        private void createListe()
        {
            object misValue = System.Reflection.Missing.Value;
            Excel.Application xlApp = new Excel.Application();
            Excel.Workbook xlWorkbook = xlApp.Workbooks.Open(textBox3.Text);
            Excel._Worksheet xlWorksheet = xlWorkbook.Sheets[1];
            Excel.Range xlRange = xlWorksheet.UsedRange;
            int rw = xlRange.Rows.Count;
            int cl = xlRange.Columns.Count;
            for (int i = 1; i <= rw; i++)
            {
                string str1 = Convert.ToString((xlRange.Cells[i, int.Parse(Properties.Settings.Default.ColCod)] as Excel.Range).Value2);
                str1 = str1 != null ? str1.Trim() : str1;
                if (!lista_XL.Contains(str1))
                {
                    lista_XL.Add(str1);
                }
            }
            Marshal.ReleaseComObject(xlRange);
            Marshal.ReleaseComObject(xlWorksheet);
            xlWorkbook.Close(false, misValue, misValue);
            Marshal.ReleaseComObject(xlWorkbook);
            xlApp.Quit();
            Marshal.ReleaseComObject(xlApp);
            foreach (string file in files)
            {
                string pdf_id = file.Substring(file.LastIndexOf('\\') + 1).Trim();
                pdf_id = pdf_id.Replace("-", "_");
                pdf_id = pdf_id.Substring(0, pdf_id.IndexOf('_'));
                lista_PDF.Add(pdf_id);

            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            progressBar1.Value = 0;
            files = Directory.GetFiles(textBox1.Text).AsEnumerable().Where(a => a.Contains(".pdf")).ToList();
            int count = 0;
            foreach (var file in files)
            {
                if (file.Contains(".pdf"))
                {
                    count++;
                }
            }
            progressBar1.Maximum = count;
            if (count == 0)
            {
                MessageBox.Show("Nu a fost gasit nici un fisier pdf !", "ATENTIE", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (!backgroundWorker1.IsBusy)
            {
                Globalz.LoadingGif(this.button1);
                backgroundWorker1.RunWorkerAsync();
            }
        }

        private void LoadCombo()
        {
            for (int i = 1; i <= 22; i++)
            {
                comboBox1.Items.Add(i);
            }
            if (!string.IsNullOrWhiteSpace(Properties.Settings.Default.ColCod.ToString()))
            {
                comboBox1.SelectedItem = int.Parse(Properties.Settings.Default.ColCod);
            }
            else
            {
                comboBox1.SelectedIndex = 0;
            }
        }

        private void checkbtn()
        {
            if (textBox1.Text.Trim() == "" || textBox3.Text.Trim() == "")
            {
                button1.Enabled = false;
            }
            else
            {
                button1.Enabled = true;
            }
        }
        private void Compare_ID_Load(object sender, EventArgs e)
        {
            LoadCombo();
            lista_XL = new List<string>();
            lista_PDF = new List<string>();
            textBox1.Text = Properties.Settings.Default.GetPathID;
            textBox3.Text = Properties.Settings.Default.XLPathID;
            desktop_path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            checkbtn();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            using (var fbd = new FolderBrowserDialog())
            {
                DialogResult result = fbd.ShowDialog();
                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                {
                    textBox1.Text = fbd.SelectedPath;
                    Properties.Settings.Default.GetPathID = fbd.SelectedPath;
                    Properties.Settings.Default.Save();
                }
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            using (var fbd = new OpenFileDialog())
            {
                fbd.Filter = "Excel Files|*.xls;*.xlsx;*.xlsm";
                DialogResult result = fbd.ShowDialog();
                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.FileName))
                {
                    textBox3.Text = fbd.FileName;
                    Properties.Settings.Default.XLPathID = fbd.FileName;
                    Properties.Settings.Default.Save();
                }
            }
        }

        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            Globalz.del_gif(this);
            if (e.ProgressPercentage <= progressBar1.Maximum)
                progressBar1.Value = e.ProgressPercentage;
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            Globalz.del_gif(this);
            progressBar1.Value = 0;
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox c = (ComboBox)sender;
            Properties.Settings.Default.ColCod = c.SelectedItem.ToString();
            Properties.Settings.Default.Save();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            checkbtn();
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            checkbtn();
        }
    }
}
