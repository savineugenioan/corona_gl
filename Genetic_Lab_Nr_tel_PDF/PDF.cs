﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;

namespace Genetic_Lab_Nr_tel_PDF
{
    public partial class PDF : Form
    {
        Dictionary<string, string[]> lista;
        string[] files;
        string outputFolder;
        string desktop_path;
        public PDF()
        {
            InitializeComponent();
            backgroundWorker1.DoWork += new DoWorkEventHandler(bkg);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            LoadCombo();
            lista = new Dictionary<string, string[]>();
            textBox1.Text = Properties.Settings.Default.GetPath;
            textBox3.Text = Properties.Settings.Default.XLPath;
            if (textBox1.Text.Trim()=="" || textBox3.Text.Trim() == "")
            {
                button1.Enabled = false;
            }
            else
            {
                button1.Enabled = true;
            }
        }

        private void OutputFolder()
        {
            desktop_path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            string[] date = DateTime.Today.ToString("MM/dd/yyyy").Split(new string[] { "/", "-" }, StringSplitOptions.RemoveEmptyEntries);
            string x = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            string azi = date[1] + "." + date[0] + "." + date[2];
            if (Directory.Exists(desktop_path + "\\" + azi + "." + "FinalPozitivi"))
            {
                Directory.Delete(desktop_path + "\\" + azi + "." + "FinalPozitivi", true);
            }
            Directory.CreateDirectory(desktop_path + "\\" + azi + "." + "FinalPozitivi");
            outputFolder = desktop_path + "\\" + azi + "." + "FinalPozitivi";
        }

        private void LoadCombo()
        {
            for (int i = 1; i <= 22; i++)
            {
                comboBox1.Items.Add(i);
                comboBox2.Items.Add(i);
            }
            if (Properties.Settings.Default.ColCod != null && Properties.Settings.Default.ColCod != "")
            {
                comboBox1.SelectedItem = int.Parse(Properties.Settings.Default.ColCod);
            }
            else
            {
                comboBox1.SelectedIndex = 0;
            }
            if (Properties.Settings.Default.CodTel != null && Properties.Settings.Default.CodTel != "")
            {
                comboBox2.SelectedItem = int.Parse(Properties.Settings.Default.CodTel);
            }
            else
            {
                comboBox2.SelectedIndex = 0;
            }
        }

        private bool checkPdf(string name)
        {
            return name.Contains(".pdf");
        }
        private void bkg(object sender, DoWorkEventArgs e)
        {
            try
            {
                lista.Clear();
                createList();
                OutputFolder();
                int cont = 0;
                foreach (var file in files)
                {
                    BaseFont bf = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                    if (checkPdf(file))
                    {
                        PdfReader reader = new PdfReader(file);
                        string key = file.Substring(0, file.IndexOf('_')).Substring(file.LastIndexOf('\\') + 1).Trim();
                        using (PdfStamper stamper = new PdfStamper(reader, File.Create(outputFolder + file.Substring(file.LastIndexOf("\\")))))
                        {
                            TextField tf = new TextField(stamper.Writer, new iTextSharp.text.Rectangle(300, 50, 100, 70), "Telefon")
                            {
                                FontSize = 14,
                            };
                            if (lista.ContainsKey(key) && !string.IsNullOrEmpty(lista[key][1]))
                            {
                                string tel = lista[key][1].Substring(0, 1) == "0" ? lista[key][1] : string.Concat("0", lista[key][1]);
                                tf.Text = tel;
                            }
                            else
                            {
                                File.AppendAllText(outputFolder + "\\" + "fara_numar.txt", file + Environment.NewLine);
                            }
                                stamper.AddAnnotation(tf.GetTextField(), 1);

                            var pageSize = reader.GetPageSize(1);
                            int x = 50;
                            int y = 55;
                            ColumnText.ShowTextAligned(stamper.GetOverContent(1), Element.ALIGN_BOTTOM, new Phrase("Telefon:", new iTextSharp.text.Font(bf, 14)), x, y, 0);

                            stamper.Close();
                        }
                    }
                    cont++;
                    backgroundWorker1.ReportProgress(cont);
                }
                MessageBox.Show("Fisierele au fost editate cu succes", "Succes", MessageBoxButtons.OK, MessageBoxIcon.Information);
                backgroundWorker1.ReportProgress(0);
            }
            catch (IOException exc)
            {
                MessageBox.Show("Verificati sa nu aveti PDF-uri deschise!", "EROARE IO", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "EROARE", MessageBoxButtons.OK, MessageBoxIcon.Error);
                // Get stack trace for the exception with source file information
                if(File.Exists(desktop_path + "\\" + "erori.txt"))
                {
                    File.Delete(desktop_path + "\\" + "erori.txt");
                }
                File.AppendAllText(desktop_path + "\\" + "erori.txt", ex.StackTrace + Environment.NewLine + ex.Message);
            }

        }
        private void createList()
        {
            object misValue = System.Reflection.Missing.Value;
            Excel.Application xlApp = new Excel.Application();
            Excel.Workbook xlWorkbook = xlApp.Workbooks.Open(textBox3.Text);
            Excel._Worksheet xlWorksheet = xlWorkbook.Sheets[1];
            Excel.Range xlRange = xlWorksheet.UsedRange;
            int rw = xlRange.Rows.Count;
            int cl = xlRange.Columns.Count;
            for (int i = 1; i <= rw; i++)
            {
                string str1 = Convert.ToString((xlRange.Cells[i, int.Parse(Properties.Settings.Default.ColCod)] as Excel.Range).Value2);
                string str2 = Convert.ToString((xlRange.Cells[i, 2] as Excel.Range).Value2);
                string str3 = Convert.ToString((xlRange.Cells[i, int.Parse(Properties.Settings.Default.CodTel)] as Excel.Range).Value2);
                str1 = str1 != null ? str1.Trim() : str1;
                str2 = str2 != null ? str2.Trim() : str2;
                str3 = str3 != null ? str3.Trim() : str3;
                if (!lista.ContainsKey(str1))
                {
                    lista.Add(str1, new string[] { str2, str3});
                }
            }
            Marshal.ReleaseComObject(xlRange);
            Marshal.ReleaseComObject(xlWorksheet);
            xlWorkbook.Close(false, misValue, misValue);
            Marshal.ReleaseComObject(xlWorkbook);
            xlApp.Quit();
            Marshal.ReleaseComObject(xlApp);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            progressBar1.Value = 0;
            files = Directory.GetFiles(textBox1.Text);
            int count = 0;
            foreach (var file in files)
            {
                if (file.Contains(".pdf"))
                    count++;
            }
            progressBar1.Maximum = count;
            if (count == 0)
            {
                MessageBox.Show("Nu a fost gasit nici un fisier pdf !", "ATENTIE", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (File.Exists(outputFolder + "\\" + "fara_numar.txt"))
            {
                File.Delete(outputFolder + "\\" + "fara_numar.txt");
            }
            if (!backgroundWorker1.IsBusy)
            {
                Globalz.LoadingGif(this.button1);
                backgroundWorker1.RunWorkerAsync();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            using (var fbd = new FolderBrowserDialog())
            {
                DialogResult result = fbd.ShowDialog();
                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                {
                    textBox1.Text = fbd.SelectedPath;
                    Properties.Settings.Default.GetPath = fbd.SelectedPath;
                    Properties.Settings.Default.Save();
                }
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            if (textBox1.Text.Trim() == "" || textBox3.Text.Trim() == "")
            {
                button1.Enabled = false;
            }
            else
            {
                button1.Enabled = true;
            }
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            if (textBox1.Text.Trim() == "" || textBox3.Text.Trim() == "")
            {
                button1.Enabled = false;
            }
            else
            {
                button1.Enabled = true;
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            using (var fbd = new OpenFileDialog())
            {
                DialogResult result = fbd.ShowDialog();
                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.FileName))
                {
                    textBox3.Text = fbd.FileName;
                    Properties.Settings.Default.XLPath = fbd.FileName;
                    Properties.Settings.Default.Save();
                }
            }
        }

        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            Globalz.del_gif(this);
            if(e.ProgressPercentage<=progressBar1.Maximum)
                progressBar1.Value = e.ProgressPercentage;
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            Globalz.del_gif(this);
            progressBar1.Value = 0;
        }
        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox c = (ComboBox)sender;
            Properties.Settings.Default.ColCod = c.SelectedItem.ToString();
            Properties.Settings.Default.Save();
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox c = (ComboBox)sender;
            Properties.Settings.Default.CodTel = c.SelectedItem.ToString();
            Properties.Settings.Default.Save();
        }
    }
}
