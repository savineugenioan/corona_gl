﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Genetic_Lab_Nr_tel_PDF
{
    public class Globalz
    {
       public static void LoadingGif(object y)
        {
            try
            {
                Control x = (Control)y;
                Form f = x.FindForm();
                PictureBox gif = new PictureBox();
                gif.Image = Properties.Resources.loader_gif_transparent_background_1;
                gif.Location = new Point(x.Location.X -75, x.Location.Y);
                gif.SizeMode = PictureBoxSizeMode.StretchImage;
                gif.Size = new Size(x.Height+5, x.Height+5);
                f.Controls.Add(gif);
            }
            catch { };
        }
        public static void del_gif(Form f)
        {
            foreach (PictureBox item in f.Controls.OfType<PictureBox>())
                if (item.Name == "")
                {
                    f.Controls.Remove(item);
                }
        }
    }
}
