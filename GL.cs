﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Genetic_Lab_Nr_tel_PDF;
using Fisiere_pozitivi_negativi;
using cOMPARE_iD;
using Numar_PDF;
using CompareXluri;

namespace Corona_GL
{
    public partial class GL : Form
    {
        public GL()
        {
            InitializeComponent();
        }

        private void Genetic_Lab_Nr_tel_PDF_Click(object sender, EventArgs e)
        {
            Form f = new Genetic_Lab_Nr_tel_PDF.PDF();
            f.ShowDialog();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form f = new Fisiere_pozitivi_negativi.Fis_po_neg();
            f.ShowDialog();
        }
        private void button2_Click(object sender, EventArgs e)
        {
            Form f = new Numar_PDF.Numar_PDF();
            f.ShowDialog();
        }

        private void GL_Load(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            Form f = new cOMPARE_iD.Compare_ID();
            f.ShowDialog();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Form f = new ComparareXLuri();
            f.ShowDialog();
        }
    }
}
