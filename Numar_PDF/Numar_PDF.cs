﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Numar_PDF
{
    public partial class Numar_PDF : Form
    {
        public Numar_PDF()
        {
            InitializeComponent();
        }

        private void Numar_PDF_Load(object sender, EventArgs e)
        {
            textBox1.Text = Properties.Settings.Default.GetPath;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text.Trim() == "")
            {
                MessageBox.Show("Selectati un folder!", "ATENTIE", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            string[] files = Directory.GetFiles(textBox1.Text);
            int count = 0;
            foreach (var file in files)
            {
                if (file.Contains(".pdf"))
                    count++;
            }
           MessageBox.Show($"Folderul selectat contine {count} PDF-uri", "Succes", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            using (var fbd = new FolderBrowserDialog())
            {
                DialogResult result = fbd.ShowDialog();
                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                {
                    textBox1.Text = fbd.SelectedPath;
                    Properties.Settings.Default.GetPath = fbd.SelectedPath;
                    Properties.Settings.Default.Save();
                }
            }
        }
    }
}
